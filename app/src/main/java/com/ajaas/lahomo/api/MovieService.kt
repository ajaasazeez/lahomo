package com.ajaas.lahomo.api

import android.util.Log
import com.ajaas.lahomo.API_KEY
import com.ajaas.lahomo.BASE_URL
import com.ajaas.lahomo.LANGUAGE
import com.ajaas.lahomo.data.models.Movie
import com.ajaas.lahomo.data.models.MoviesResponseModel
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


fun getMovies(
    service: MovieService,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (repos: List<Movie>) -> Unit,
    onError: (error: String) -> Unit
){
    service.getMovies(
        apiKey = API_KEY,
        language = LANGUAGE,
        page = page
    ).enqueue(
        object : retrofit2.Callback<MoviesResponseModel> {
            override fun onFailure(call: Call<MoviesResponseModel>?, t: Throwable) {
                Log.d("retrofit", "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<MoviesResponseModel>?,
                response: Response<MoviesResponseModel>
            ) {
                Log.d("retrofit", "got a response $response")
                if (response.isSuccessful) {
                    val movies = response.body()?.results ?: emptyList()
                    onSuccess(movies)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}
interface MovieService{

        /**
         * Get repos ordered by stars.
         */
        @GET("movie/now_playing?")
        fun getMovies(
            @Query("api_key") apiKey: String,
            @Query("language") language: String,
            @Query("page") page: Int
        ): Call<MoviesResponseModel>

        companion object {

        fun create(): MovieService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MovieService::class.java)
        }
    }

}