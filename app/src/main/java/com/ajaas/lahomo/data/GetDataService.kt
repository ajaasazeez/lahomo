package com.bestdocapp.kioskv4.data.remote

import com.ajaas.lahomo.data.models.MoviesResponseModel
import retrofit2.Response
import retrofit2.http.*

interface GetDataService {

@GET("movie/now_playing?")
suspend fun getMovies(
    @Query("api_key") apiKey: String,
    @Query("language") language: String,
    @Query("page") page: Int
): Response<MoviesResponseModel>?

}



