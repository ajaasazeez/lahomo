package com.ajaas.lahomo.data.db

import android.util.Log
import androidx.lifecycle.LiveData
import com.ajaas.lahomo.data.models.Movie
import java.util.concurrent.Executor
/**
 * Class that handles the DAO local data source. This ensures that methods are triggered on the
 * correct executor.
 */
class MovieLocalCache  (
    private val movieDao: MovieDao,
    private val ioExecutor: Executor
) {
    /**
     * Insert a list of repos in the database, on a background thread.
     */
    fun insert(movies: List<Movie>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("MovieLocalCache", "inserting ${movies.size} repos")
            movieDao.insert(movies)
            insertFinished()
        }
    }

    fun getMovies(): LiveData<List<Movie>> {
        return movieDao.getMovies()
    }
}