package com.ajaas.lahomo.data.models

import androidx.lifecycle.LiveData

data class MovieResult  (
    val data: LiveData<List<Movie>>,
    val networkErrors: LiveData<String>
)