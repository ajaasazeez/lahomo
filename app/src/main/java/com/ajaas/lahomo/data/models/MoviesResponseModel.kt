package com.ajaas.lahomo.data.models

import com.google.gson.annotations.SerializedName

data class MoviesResponseModel (
    @SerializedName("results") val results : List<Movie>,
    @SerializedName("page") val page : Int,
    @SerializedName("total_results") val total_results : Int,
    @SerializedName("total_pages") val total_pages : Int
){

}