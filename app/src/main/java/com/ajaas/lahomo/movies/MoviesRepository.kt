package com.ajaas.lahomo.movies

import com.ajaas.lahomo.API_KEY
import com.ajaas.lahomo.LANGUAGE
import com.bestdocapp.kioskv4.data.remote.ApiFactory
import com.bestdocapp.kioskv4.data.remote.GetDataService
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException

class MoviesRepository {

    suspend fun getMovies(
    page: Int
    ){
        withContext(Dispatchers.IO) {
            try {
                val service = ApiFactory.retrofit().create(GetDataService::class.java)
                val call = service.getMovies(
                    apiKey = API_KEY,
                    language = LANGUAGE,
                    page = page

                )
                if (call?.isSuccessful!!) {
                    call.body().let { moviesResponse ->

                    }

                }

            }
            catch (exception: IOException) {
                println(exception)
            }

        }
    }
}